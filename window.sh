#!/bin/bash

set +e

X_SIZE=960 #700
Y_SIZE=540 #530
P_SIZE=30

if [ "$1" == "q" ]; then
  x=0       y=0       x_s=$X_SIZE            y_s=$(( $Y_SIZE - $P_SIZE ))
elif [ "$1" == "w" ]; then
  x=0       y=0       x_s=$(( $X_SIZE * 2 )) y_s=$(( $Y_SIZE - $P_SIZE ))
elif [ "$1" == "e" ]; then
  x=$X_SIZE y=0       x_s=$X_SIZE            y_s=$(( $Y_SIZE - $P_SIZE ))
elif [ "$1" == "a" ]; then
  x=0       y=0       x_s=$X_SIZE            y_s=$(( $Y_SIZE * 2 - $P_SIZE ))
elif [ "$1" == "s" ]; then
  x=0       y=0       x_s=$(( $X_SIZE * 2 )) y_s=$(( $Y_SIZE * 2 - $P_SIZE ))
elif [ "$1" == "d" ]; then
  x=$X_SIZE y=0       x_s=$X_SIZE            y_s=$(( $Y_SIZE * 2 - $P_SIZE ))
elif [ "$1" == "z" ]; then
  x=0       y=$(( $Y_SIZE + $P_SIZE )) x_s=$X_SIZE            y_s=$(( $Y_SIZE - $P_SIZE ))
elif [ "$1" == "x" ]; then
  x=0       y=$(( $Y_SIZE + $P_SIZE )) x_s=$(( $X_SIZE * 2 )) y_s=$(( $Y_SIZE - $P_SIZE ))
elif [ "$1" == "c" ]; then
  x=$X_SIZE y=$(( $Y_SIZE + $P_SIZE )) x_s=$X_SIZE            y_s=$(( $Y_SIZE - $P_SIZE ))
fi
  
xdotool getactivewindow windowmove $x $y
xdotool getactivewindow windowsize $x_s $y_s
