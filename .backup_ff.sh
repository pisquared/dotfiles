#!/bin/bash

set -x
set -e

MOUNT_DIR=`pwd`
killall /usr/lib/firefox/firefox
sleep 2
RELEASE_FOLDER=`find ~/.mozilla/firefox/ -name *.default-release`
rm -rf "${MOUNT_DIR}/.firefox_profile"
cp -r $RELEASE_FOLDER "${MOUNT_DIR}/.firefox_profile"
rm -rf "${MOUNT_DIR}/.firefox_profile/datareporting/archived"
firefox & disown
