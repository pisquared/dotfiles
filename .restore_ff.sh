#!/bin/bash

set -x
set -e

MOUNT_DIR=`pwd`
killall /usr/lib/firefox/firefox
rm -rf ~/.mozilla/firefox/
firefox & disown
sleep 2
killall /usr/lib/firefox/firefox
RELEASE_FOLDER=`find ~/.mozilla/firefox/ -name *.default-release`
rm -rfv "$RELEASE_FOLDER"
cp -r "${MOUNT_DIR}/.firefox_profile" $RELEASE_FOLDER
firefox & disown
