scriptencoding utf-8
set encoding=utf-8
set nocompatible              " be iMproved, required
filetype off                  " required

syntax enable           " enable syntax processing
set expandtab
set shiftwidth=2
set softtabstop=2
set expandtab       " tabs are spaces
set number relativenumber       " show hybrid relative line numbers
set showmatch           " highlight matching [{()}]
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
set smartindent
filetype plugin indent on
highlight ColorColumn ctermbg=235 guibg=#2c2d27
let &colorcolumn="80,".join(range(120,999),",")
set shellcmdflag=-ic
