# i3status configuration file.
# see "man i3status" for documentation.

# It is important that this file is edited as UTF-8.
# The following line should contain a sharp s:
# ß
# If the above line is not correctly displayed, fix your editor first!

general {
        colors = true
        interval = 1
}

order += "sysdata"
#order += "diskdata"
#order += "net_rate"
#order += "online_status"
order += "battery_level"
order += "volume master"
#order += "keyboard_layout"
order += "tztime local"


sysdata {
  format = 'M:[\?color=mem {mem_used}G {mem_used_percent}%] C:[\?color=cpu {cpu_usage}%] L:[\?color=load {load1}]'
}

diskdata {
  format = "D[\?color=read R:{read}] [\?color=write W:{write}]"
  format_rate = "[\?min_length=4 {value:.1f}M]"
  unit = "MB/s"
}

net_rate {
  format = "N[\?color=up U:{up}] [\?color=down D:{down}]"
  format_value = "[\?min_length=4 {value:.1f}M]"
  thresholds = "[(4*1024*1024, 'bad'), (1024*1024, 'degraded'), (0, 'good')]"
  unit = "MB/s"
}

battery_level {
  format = "{icon} {percent}% {time_remaining}"
}

keyboard_layout {
  layouts = ['bg', 'us']
}

tztime local {
  format = "%Y-%m-%d %H:%M:%S"
}

volume master {
  format = "♪: %volume"
  format_muted = "♪: MUT (%volume)"
  mixer = "Master"
}
