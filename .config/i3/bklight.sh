#!/bin/bash

MAX_BRIGHTNESS=$(cat /sys/class/backlight/intel_backlight/max_brightness)
let "STEP = $MAX_BRIGHTNESS / 10"

CURRENT_BRIGHTNESS=$(cat /sys/class/backlight/intel_backlight/brightness)

action="${!#}"
if [ "$action" == "up" ]; then
 let "NEW_BRIGHTNESS = CURRENT_BRIGHTNESS + STEP"
elif [ "$action" == "down" ]; then
 let "NEW_BRIGHTNESS = CURRENT_BRIGHTNESS - STEP"
else
  exit 1;
fi

sudo sh -c "echo $NEW_BRIGHTNESS > /sys/class/backlight/intel_backlight/brightness"

