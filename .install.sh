#!/usr/bin/env bash
# TODO: sync eternal history


echo "Adding dotfiles as home repo"
eval `ssh-agent`
ssh-add ~/.ssh/pi2_key_external
ssh-keyscan gitlab.com >> ~/.ssh/known_hosts


echo -e "\nsource ~/.bashrc.pi2" | tee --append .bashrc

echo 
echo "========================================================"
echo "Enable sudoless account"
echo
echo "pi2 ALL=(ALL) NOPASSWD: ALL" | sudo tee --append /etc/sudoers

echo 
echo "========================================================"
echo "Update system"
echo
sudo apt update

echo 
echo "========================================================"
echo "Install packages"
echo
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | sudo debconf-set-selections
sudo apt install -y i3 py3status \
                    tmux \
                    terminator \
                    firefox \
                    vim pluma retext meld \
                    transmission \
                    calibre \
                    libreoffice-writer libreoffice-calc \
                    vlc \
                    eog gimp inkscape \
                    evince \
                    arandr \
                    feh \
                    dconf-editor \
                    redshift-gtk \
                    autokey-gtk \
                    git \
                    htop glances iftop iotop \
                    xdotool \
                    curl \
                    xclip autocutsel \
                    nmap tcpdump \
                    tree \
                    openssh-server \
                    software-properties-common ubuntu-restricted-extras \
                    python3-pip python3-virtualenv \
                    zip unzip p7zip p7zip-rar rar unrar \
                    samba \
                    ffmpeg \
                    sqlite \
                    indicator-multiload \
                    lxappearance \
                    acpi \
                    xarchiver \
                    gvfs-backends gvfs-common gvfs-fuse \
                    cifs-utils \
                    xournal \
                    simplescreenrecorder peek \
                    audacity kdenlive kolourpaint obs-studio \
                    jq \
                    pm-utils

sudo mkdir -p /media/data
sudo mkdir -p /media/share


echo "Removing default autokey files"
rm -rf .config/autokey/data/My\ Phrases/
rm -rf .config/autokey/data/Sample\ Scripts/

echo 
echo "========================================================"
echo "Install ipython 2/3 and youtube-dl"
echo
sudo pip3 install --upgrade pip
sudo pip3 install ipython youtube-dl 


echo "install owncloud client"
sudo sh -c "echo 'deb http://download.opensuse.org/repositories/isv:/ownCloud:/desktop:/daily:/2.6/Ubuntu_20.04/ /' > /etc/apt/sources.list.d/isv:ownCloud:desktop:daily:2.6.list"
wget -qO - https://download.opensuse.org/repositories/isv:ownCloud:desktop:daily:2.6/Ubuntu_20.04/Release.key | apt-key add -
sudo apt update
sudo apt install -y owncloud-client

echo 
echo "========================================================"
echo "install webtorrent desktop"
echo
wget -O /tmp/webtorrent.deb "https://github.com/webtorrent/webtorrent-desktop/releases/download/v0.21.0/webtorrent-desktop_0.21.0_amd64.deb"
sudo dpkg -i /tmp/webtorrent.deb

wget -O /tmp/jetbrains.tar.gz "https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.17.6856.tar.gz"
mkdir -p /tmp/jetbrains
tar xfvz /tmp/jetbrains.tar.gz -C /tmp/jetbrains
/tmp/jetbrains/jetbrains-toolbox-1.17.6856/jetbrains-toolbox & disown

echo 
echo "========================================================"
echo "Pull APOD and set it in crotab"
echo
mkdir -p ~/workspace
git clone git@gitlab.com:pisquared/apod-wallpaper.git ~/workspace/apod-wallpaper
crontab -l 2>/dev/null >/tmp/_cron
echo "0 * * * * export DISPLAY=:0 && cd $HOME/workspace/apod-wallpaper && ./apod.sh" >> /tmp/_cron
echo "@reboot export DISPLAY=:0 && cd $HOME/workspace/apod-wallpaper && ./apod.sh" >> /tmp/_cron
crontab /tmp/_cron

sudo crontab -l 2>/dev/null >/tmp/_cron
echo "0 * * * * apt-get update && apt-get -y dist-upgrade" >> /tmp/_cron
sudo crontab /tmp/_cron
# set apod
cd $HOME/workspace/apod-wallpaper && ./apod.sh
cd

git clone git@gitlab.com:pisquared/oshipka.git ~/workspace/oshipka
cd ~/workspace/oshipka && v && a && pip install -r requirements.txt
git clone git@gitlab.com:pisquared/tww.git ~/workspace/tww
cd ~/workspace/tww && v && a && pip install -r requirements.txt

sudo bash -c 'echo XKBOPTIONS="terminate:ctrl_alt_bksp" >> /etc/default/keyboard'

echo 
echo "========================================================"
echo "Remove screensaver"
echo
xset s off
xset -dpms
sudo sed -i "s/#HandlePowerKey=.*/HandlePowerKey=ignore/g" /etc/systemd/logind.conf

echo 
echo "========================================================"
echo "Autoremove packages"
echo
sudo apt -y dist-upgrade
sudo apt -y autoremove

rm -rf ~/Documents/ ~/Public ~/Templates ~/Desktop ~/Music
cd ~
ln -s /media/data/owncloud_local/documents/ Documents
ln -s /media/data/owncloud_local/current_music/ Music
ln -s /media/data/owncloud_local/ oc

echo 
echo "========================================================"
echo "Disable and stop smbd sshd"
echo
echo 
sudo systemctl disable smbd sshd
sudo systemctl stop smbd sshd

echo "========================================================"
echo "Samba sharing"
echo
echo -n "Do you want to enable samba sharing of ~/Videos (y/N)?"
read answer

if [ $answer == "y" || $answer == 'Y' ]; then
sudo bash -c "cat >> /etc/samba/smb.conf" <<EOF

[Videos]
path = /home/pi2/Videos
guest ok = no
read only = yes
EOF

sudo smbpasswd -a pi2

sudo systemctl restart smbd 
fi

# AUTO MOUNT NFS SHARE
# ====================
# /etc/fstab
# UUID=<blkid>	/media/data	ext4	defaults	0	0
# //pi2-rpi-4.local/share	/media/share	cifs	credentials=/etc/win-credentials,file_mode=0755,dir_mode=0755 0       0
# 
# /etc/win-credentials
# username = pi2
# password = ...


sudo bash -c "cat >> /etc/ssh/sshd_config" <<'EOF'
AllowUsers pi2
Port 4226
ChallengeResponseAuthentication no
PasswordAuthentication no
UsePAM no
EOF

sudo systemctl restart ssh

cat .ssh/pi2_key_external.pub > .ssh/authorized_keys
chmod 700 .ssh
chmod 600 .ssh/authorized_keys

sudo ufw enable
sudo ufw allow from 192.168.1.0/24 to any port 4226

# SETUP PIHOLE DNS
# ================
# sudo nmcli con mod "Wired connection 1" ipv4.dns "192.168.1.110"
# sudo nmcli con mod "Wired connection 1" ipv4.ignore-auto-dns yes
# sudo nmcli con down "Wired connection 1" && sudo nmcli con up "Wired connection 1"

